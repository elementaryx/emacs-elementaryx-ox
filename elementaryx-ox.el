;; (use-package elementaryx-ox-latex-minimal) ;; already required by elementaryx-ox-beamer-minimal and elementaryx-ox-latex
(use-package elementaryx-ox-beamer-minimal)
(use-package elementaryx-ox-latex)
(use-package elementaryx-ox-html)
(use-package org-re-reveal :after ox) ;; TODO: shall we refine it into (use-package elementaryx-ox-re-reveal) ?

(provide 'elementaryx-ox)
